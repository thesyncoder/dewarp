FROM pytorch/pytorch:1.5.1-cuda10.1-cudnn7-runtime

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev && apt-get install -y git && \
    apt-get install ffmpeg libsm6 libxext6  -y

EXPOSE 5000

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

RUN git clone https://github.com/DVLP-CMATERJU/RectiNet.git

RUN gdown --id 1Q_cWtIX-quCizX8huznfcIYJKxCOkTSZ

COPY . /app

CMD [ "python", "./api.py" ]

