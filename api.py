import re
from flask import Flask,  render_template, request, redirect, url_for
from werkzeug.utils import secure_filename

import logging
from logging import Formatter, FileHandler
import os
from model import dewarp

app = Flask(__name__)
UPLOAD_FOLDER = 'static/images'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route("/")
def home():
    return render_template('pages/welcome.html')


@app.route("/predict")
def predict():
    img = None
    dewarp = None 
    if 'img' in request.args:
        img = request.args['img']
    if 'dewarp' in request.args:
        dewarp = request.args['dewarp']
    return render_template('pages/predict.html', img=img, dewarp=dewarp)


@app.route("/upload", methods=["POST"])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            return redirect(request.url)
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            dewarp(filename)
            file_parts = str(filename).split('.')
            return redirect(url_for('predict', img=os.path.join(app.config['UPLOAD_FOLDER'], filename), dewarp=os.path.join(app.config['UPLOAD_FOLDER'], file_parts[0] + 'dewarp.png' )))


@app.errorhandler(500)
def internal_error(error):
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

#----------------------------------------------------------------------------#
# Launch.
#----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':
    app.run(host='0.0.0.0')
